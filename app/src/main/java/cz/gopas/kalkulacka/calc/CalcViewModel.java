package cz.gopas.kalkulacka.calc;

import android.app.Application;
import android.content.SharedPreferences;
import android.util.Log;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.preference.PreferenceManager;

import cz.gopas.kalkulacka.R;

public class CalcViewModel extends AndroidViewModel {
	public CalcViewModel(@NonNull Application application) {
		super(application);
	}

	private static final String TAG = CalcViewModel.class.getSimpleName();
	private static final String ANS_KEY = "ans";

	final private MutableLiveData<Float> result_ = new MutableLiveData<>(null);
	final public LiveData<Float> result = result_;

	private final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplication());

	public void calc(@IdRes int op, float a, float b) {
		new Thread(() -> {
			try {
				Log.d(TAG, "calc start");
				Thread.sleep(500);
				float res = Float.NaN;
				switch (op) {
					case R.id.add:
						res = a + b;
						break;
					case R.id.sub:
						res = a - b;
						break;
					case R.id.mul:
						res = a * b;
						break;
					case R.id.div:
						res = a / b;
						break;
				}
				prefs.edit().putFloat(ANS_KEY, res).apply();
				result_.postValue(res);
				Log.d(TAG, "calc end");
			} catch (InterruptedException e) {
				Log.w(TAG, e);
			}
		}).start();
	}

	public float getAns() {
		return prefs.getFloat(ANS_KEY, Float.NaN);
	}
}
