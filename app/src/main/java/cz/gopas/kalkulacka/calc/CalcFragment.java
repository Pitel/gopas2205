package cz.gopas.kalkulacka.calc;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import cz.gopas.kalkulacka.R;
import cz.gopas.kalkulacka.databinding.FragmentCalcBinding;
import cz.gopas.kalkulacka.history.HistoryFragment;
import cz.gopas.kalkulacka.history.HistoryViewModel;
import cz.gopas.kalkulacka.history.db.HistoryItem;

public class CalcFragment extends Fragment {
	private static final String TAG = CalcFragment.class.getSimpleName();
	private static final String RES_KEY = "res";

	private FragmentCalcBinding binding;

	private CalcViewModel calcViewModel;
	private HistoryViewModel historyViewModel;

	@Override
	public void onAttach(@NonNull Context context) {
		super.onAttach(context);
		calcViewModel = new ViewModelProvider(this).get(CalcViewModel.class);
		historyViewModel = new ViewModelProvider(requireActivity()).get(HistoryViewModel.class);
	}

	@Override
	public View onCreateView(
			@NonNull LayoutInflater inflater,
			@Nullable ViewGroup container,
			@Nullable Bundle savedInstanceState
	) {
		binding = FragmentCalcBinding.inflate(inflater, container, false);
		return binding.getRoot();
	}

	@Override
	public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
		binding.calc.setOnClickListener(v -> calc());
		binding.share.setOnClickListener(v -> share());
		binding.ans.setOnClickListener(v ->
				binding.a.getEditText().setText(String.valueOf(calcViewModel.getAns()))
		);
		binding.history.setOnClickListener(v -> getParentFragmentManager().beginTransaction()
				.replace(R.id.fragment_container, new HistoryFragment())
				.addToBackStack(null)
				.commit()
		);
		calcViewModel.result.observe(getViewLifecycleOwner(), result -> {
			if (result != null) {
				binding.res.setText(String.valueOf(result));
				historyViewModel.add(new HistoryItem(result));
			}
		});
		historyViewModel.selected.observe(getViewLifecycleOwner(), selected -> {
			if (selected != null) {
				binding.b.getEditText().setText(String.valueOf(selected));
				historyViewModel.selected.postValue(null);
			}
		});
	}

	@Override
	public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
		super.onViewStateRestored(savedInstanceState);
		if (savedInstanceState != null) {
			binding.res.setText(savedInstanceState.getCharSequence(RES_KEY));
		}
	}

	@Override
	public void onSaveInstanceState(@NonNull Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putCharSequence(RES_KEY, binding.res.getText());
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		binding = null;
	}

	private void calc() {
		Log.d(TAG, "Calc");
		float a = Float.NaN;
		try {
			a = Float.parseFloat(binding.a.getEditText().getText().toString());
		} catch (Throwable t) {
			Log.w(TAG, t);
		}
		float b = Float.NaN;
		try {
			b = Float.parseFloat(binding.b.getEditText().getText().toString());
		} catch (Throwable t) {
			Log.w(TAG, t);
		}
		calcViewModel.calc(binding.ops.getCheckedRadioButtonId(), a, b);
	}

	private void share() {
		Log.d(TAG, "Share");
		final Intent intent = new Intent(Intent.ACTION_SEND)
				.setType("text/plain")
				.putExtra(
						Intent.EXTRA_TEXT,
						getString(R.string.share_text, binding.res.getText())
				);
		startActivity(intent);
	}
}