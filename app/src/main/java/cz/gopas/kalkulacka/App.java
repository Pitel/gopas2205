package cz.gopas.kalkulacka;

import android.app.Application;

import androidx.fragment.app.FragmentManager;

import com.google.android.material.color.DynamicColors;

public class App extends Application {
	@Override
	public void onCreate() {
		super.onCreate();
		DynamicColors.applyToActivitiesIfAvailable(this);
		FragmentManager.enableDebugLogging(BuildConfig.DEBUG);
	}
}
