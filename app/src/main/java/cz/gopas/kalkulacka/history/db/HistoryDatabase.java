package cz.gopas.kalkulacka.history.db;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {HistoryItem.class}, version = 1)
public abstract class HistoryDatabase extends RoomDatabase {
	public abstract HistoryDao dao();
}
