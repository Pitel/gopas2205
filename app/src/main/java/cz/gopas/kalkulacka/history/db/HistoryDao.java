package cz.gopas.kalkulacka.history.db;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface HistoryDao {
	@Insert
	void add(HistoryItem... item);

	@Query("SELECT * FROM HistoryItem ORDER BY id DESC")
	LiveData<List<HistoryItem>> getAll();
}
