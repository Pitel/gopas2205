package cz.gopas.kalkulacka.history.db;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class HistoryItem {
	public HistoryItem() {}
	public HistoryItem(float value) {
		this.value = value;
	}

	@PrimaryKey(autoGenerate = true)
	public long id = 0;
	public float value;
}
