package cz.gopas.kalkulacka.history;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;

import cz.gopas.kalkulacka.R;

public class HistoryFragment extends Fragment {
	public HistoryFragment() {
		super(R.layout.fragment_history);
	}

	private static final String TAG = HistoryFragment.class.getSimpleName();

	private HistoryViewModel viewModel;

	@Override
	public void onAttach(@NonNull Context context) {
		super.onAttach(context);
		viewModel = new ViewModelProvider(requireActivity()).get(HistoryViewModel.class);
	}

	@Override
	public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
		final HistoryAdapter adapter = new HistoryAdapter(clicked -> {
			viewModel.selected.setValue(clicked);
			getParentFragmentManager().popBackStack();
		});
		final RecyclerView recycler = (RecyclerView) view;
		recycler.setAdapter(adapter);
		recycler.setHasFixedSize(true);
		viewModel.history.observe(getViewLifecycleOwner(), history -> {
			Log.d(TAG, String.valueOf(history.size()));
			adapter.submitList(history);
		});
	}
}
