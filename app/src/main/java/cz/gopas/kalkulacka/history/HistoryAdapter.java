package cz.gopas.kalkulacka.history;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import java.util.function.Consumer;

import cz.gopas.kalkulacka.history.db.HistoryItem;

public class HistoryAdapter extends ListAdapter<HistoryItem, HistoryAdapter.HistoryViewHolder> {
	public HistoryAdapter(Consumer<Float> onClick) {
		super(new DiffUtil.ItemCallback<HistoryItem>() {
			@Override
			public boolean areItemsTheSame(@NonNull HistoryItem oldItem, @NonNull HistoryItem newItem) {
				return oldItem.id == newItem.id;
			}

			@Override
			public boolean areContentsTheSame(@NonNull HistoryItem oldItem, @NonNull HistoryItem newItem) {
				return oldItem.value == newItem.value;
			}
		});

		setHasStableIds(true);
		this.onClick = onClick;
	}

	private static final String TAG = HistoryAdapter.class.getSimpleName();

	private final Consumer<Float> onClick;

	@Override
	public long getItemId(int position) {
		return getItem(position).id;
	}

	@NonNull
	@Override
	public HistoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
		Log.d(TAG, "create");
		return new HistoryViewHolder(
				LayoutInflater.from(parent.getContext()).inflate(
						android.R.layout.simple_list_item_1,
						parent,
						false
				)
		);
	}

	@Override
	public void onBindViewHolder(@NonNull HistoryViewHolder holder, int position) {
		Log.d(TAG, "bind");
		final float value = getItem(position).value;
		holder.valueText.setText(String.valueOf(value));
		holder.itemView.setOnClickListener(v -> onClick.accept(value));
	}

	public static class HistoryViewHolder extends RecyclerView.ViewHolder {
		public HistoryViewHolder(@NonNull View itemView) {
			super(itemView);
		}

		public TextView valueText = itemView.findViewById(android.R.id.text1);
	}
}
