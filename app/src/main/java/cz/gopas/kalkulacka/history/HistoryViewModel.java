package cz.gopas.kalkulacka.history;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.room.Room;

import java.util.List;

import cz.gopas.kalkulacka.history.db.HistoryDatabase;
import cz.gopas.kalkulacka.history.db.HistoryItem;

public class HistoryViewModel extends AndroidViewModel {
	public HistoryViewModel(@NonNull Application application) {
		super(application);
	}

	private HistoryDatabase db = Room.databaseBuilder(
			getApplication(),
			HistoryDatabase.class,
			"history"
	).allowMainThreadQueries().build();

	public LiveData<List<HistoryItem>> history = db.dao().getAll();

	public MutableLiveData<Float> selected = new MutableLiveData<Float>(null);

	public void add(HistoryItem... items) {
		db.dao().add(items);
	}
}
